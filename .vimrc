set nocompatible        "be iMproved, required              "отключаем совместимость с vi
filetype off
"===============================================================================================================
"=== git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim/  =========install vundle===
"================ Vundle settings ==============================================================================
" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'gmarik/Vundle.vim'      " let Vundle manage Vundle, required)

"jedi-vim plugin
Plugin 'davidhalter/jedi-vim'       "Jedi-vim autocomplete plugin
Plugin 'MarcWeber/vim-addon-mw-utils'   "require vim-snipmate
Plugin 'tomtom/tlib_vim'            "require vim-snipmate
Plugin 'garbas/vim-snipmate'        "snippets
Plugin 'scrooloose/nerdtree'        "file explorer
Plugin 'airblade/vim-gitgutter'
Plugin 'fatih/vim-go'               "vim-go
call vundle#end()                   "close bundle section
"filetype on                        "Vundle required
filetype plugin on                  "Vundle required
filetype plugin indent on           "Vundle required
"___________________________________________________________________________


"=====================COLOR SCHEME==========================================
colorscheme desert
hi LineNr term=NONE cterm=NONE ctermbg=NONE ctermfg=7
hi Search ctermfg=Black ctermbg=5 cterm=NONE
"___________________________________________________________________________


set fileformat=unix
set fileencoding=utf-8
set encoding=utf-8      "The encoding displayed.

set tabstop=4           "Set the size of tabs to n-spaces   "количество пробелов, которыми символ табуляции отображается в тексте
set shiftwidth=4        "Size tab newline                   "используется для регулирование ширины отступов в пробелах, добавляемых командами >> и <<
set softtabstop=4       "Tab in insert mode                 "количество пробелов, которыми символ табуляции отображается при добавлении
set expandtab           "                                   "в режиме вставки заменяет символ табуляции на пробелы

"file type tabs
autocmd BufNewFile,BufRead *.yml set tabstop=2
autocmd BufNewFile,BufRead *.yml set softtabstop=2
autocmd BufNewFile,BufRead *.yml set shiftwidth=2

autocmd BufNewFile,BufRead *.yaml set tabstop=2
autocmd BufNewFile,BufRead *.yaml set softtabstop=2
autocmd BufNewFile,BufRead *.yaml set shiftwidth=2




set autoindent          "auto-indent to newline             "автоотступ
set number              "enable line numbering              "Влючить нумерацию строк
set wrap                "wrap long lines                    "переносить длинные строки
set linebreak           "Wrap instead of letters            "Переносить по словам вместо букв

set hlsearch            "highlight search                   "подсвечивать поиск
set ignorecase          "ignore ignore case                 "игнорируем регистр
set smartcase           "allow uppercasse find              "позволяет искать с буквами верхнего регистра
set display=lastline    "show last line on displa           "Показывать последнюю строку при перемотке в конец файла

set wildmenu
set showcmd


"set backup
"set backupdir=$HOME/.vim/backups
"set directory=$HOME/.vim/temp


set cursorline
set cursorcolumn
set t_Co=256


"set foldenable
"set foldmethod=indent   "сворачивание по отступу
"set foldmethod=marker   "by marker
"set foldmarker={,}
"set foldopen=block,hor,mark,percent,quickfix,tag
"set foldlevel=100




"Vim jump to the last position when reopening a file "при открытии файла перемещать курсор на последнюю запомненную позицию
au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$") |
            \ exe "normal g'\"" | endif


                        "Open NERDTree FE
nmap <C-W><C-T> :NERDTreeToggle<CR>

                        "Auto-brackets
"imap ( ()<LEFT>
"imap { {}<LEFT>
"imap [ []<LEFT>






            " F<xx> keys
            " Exit & Fast Exit
imap <F10> <ESC> :wq <CR>
imap <F11> <ESC> :q! <CR>
map <F11> :q! <CR>
                        "del line on insert-mode
imap <C-D> <ESC>:delete<CR>i
imap <F2> <ESC>:w <CR>i

                        "comment/uncomment out the line
vmap <F3> :s/^/#/<CR>
vmap <S-F3> :s/^#//<CR>
"multiple line
imap <F3> <Esc>:.,.s/^/#/<CR>_i
nmap <F3> :.,.s/^/#/<CR>
imap <S-F3> <Esc>:.,.s/^/#/<CR>_i
nmap <S-F3> :.,.s/^#//<CR>

" tabs shift-tab - next tab, ctrl-tab - prev tab, c+t - new tab
map <S-tab> :tabprevious<cr>
nmap <S-tab> :tabprevious<cr>
imap <S-tab> <ESC>:tabprevious<cr>i
"map <C-tab> :tabnext<cr>
"nmap <C-tab> :tabnext<cr>
"imap <C-tab> <ESC>:tabnext<cr>i
nmap <C-t> :tabnew<cr>
imap <C-t> <ESC>:tabnew<cr>

" удаление лишних пробелов перед сохранением файла
au BufWritePre,FileWritePre * let au_line=line(".")
au BufWritePre,FileWritePre * let au_col=col(".")
au BufWritePre,FileWritePre * %s/\s\+$//e
au BufWritePost * silent call cursor(au_line, au_col)



let g:jedi#use_splits_not_buffers = "left"
"let g:jedi#auto_initialization = 0
let g:jedi#show_call_signatures = "1"

let g:jedi#goto_command = "<leader>d"
let g:jedi#goto_assignments_command = "<leader>g"
let g:jedi#goto_definitions_command = ""
let g:jedi#documentation_command = "K"
let g:jedi#usages_command = "<leader>n"
let g:jedi#completions_command = "<C-Space>"
let g:jedi#rename_command = "<leader>r"
