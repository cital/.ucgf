#!/bin/bash

ln -sf ~/.ucgf/.vimrc ~/.vimrc
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim/
ln -sf ~/.ucgf/.vim/snippets ~/.vim/snippets
mkdir $HOME/.vim/backup
mkdir $HOME/.vim/temp
echo "Please start VIM and use command :PluginInstall"

